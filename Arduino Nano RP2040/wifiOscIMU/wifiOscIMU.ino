/**
 * FIXME: Add Licensing information.
 */

#include <Arduino_LSM6DSOX.h>
#include <OSCBundle.h>
#include <OSCMessage.h>
#include <SPI.h>
#include <WiFiNINA.h>
#include <WiFiUdp.h>

#include "OSCMessageAddresses.h"

constexpr pin_size_t kRedPinNumber = 22u;   /**< The PIN value of the red led. */
constexpr pin_size_t kBluePinNumber = 24u;  /**< The PIN value of the blue led. */
constexpr pin_size_t kGreenPinNumber = 23u; /**< The PIN value of the green led. */
constexpr pin_size_t kPowerPinNumber = 25u; /**< The PIN value of the power led. */

constexpr char* kWiFiSSID = "lumu";            /**< The WiFi network ssid. */
constexpr char* kWiFiPassword = "lumu2023";    /**< The WiFi network password. */
//constexpr char* kWiFiSSID = "Vodafone-A44725461";
//constexpr char* kWiFiPassword = "H3pRc9nJkXh6mYhN";
constexpr uint32_t kCheckWiFiInterval = 1000u;      /**< The milliseconds interval to check the WiFi connection. */

//const IPAddress kUDPDestinationIPAddress(192u, 168u, 1u, 6u); /**< The IP address of the destination device for UDP messages. */
const IPAddress kUDPDestinationIPAddress(192u, 168u, 4u, 1u);
const IPAddress kUDPBroadcastIPAddress(192u, 168u, 4u, 255u);
//const unsigned int kUDPDestinationPort = 9999u;             /**< The port of the destination device for UDP messages. */
//constexpr uint32_t kUDPDestinationPort = 8080u;
constexpr uint32_t kUDPDestinationPort = 8000u;
constexpr uint32_t kUDPLocalPort = 8000u;                     /**< The local port for UDP messages. */

WiFiUDP udp; /**< The object to handle UDP connections. */

float imuSensorValues[6u]; /**< The values of the IMU sensors. Acceleration values are in Hertz. Gyroscope values in units of standard gravity. */

void setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(kPowerPinNumber, OUTPUT);
  pinMode(kBluePinNumber, OUTPUT);

  // Initialize serial port.
  Serial.begin(9600u);

  //
  // Initializing WiFi soft access point.
  //

  // Check if WiFi module is available.
  if (WiFi.status() == WL_NO_MODULE)
  {
    Serial.println("No WiFi module found!");

    // Turn on the LED on to indicate the error.
    digitalWrite(LED_BUILTIN, HIGH);

    // Suspend the program execution.
    while (true);
  }
  Serial.println("WiFi module found.");

  // Connecting to WiFi network.
  StartWiFiConnection();
  Serial.print("Network SSID: ");
  Serial.print(WiFi.SSID());
  Serial.println(".");
  Serial.print("IP Address: ");
  Serial.print(WiFi.localIP());
  Serial.println(".");
  Serial.print("Signal strength (RSSI): ");
  Serial.print(WiFi.RSSI());
  Serial.println(" dBm.");

  //
  // Initializing UDP client.
  //

  if (!udp.begin(kUDPLocalPort))
  {
    Serial.println("Unable to initialize UDP socket!");
    digitalWrite(LED_BUILTIN, HIGH);
    while (true);
  }
  Serial.print("UDP socket initialized on port ");
  Serial.print(kUDPLocalPort);
  Serial.println(".");

  //
  // Initializing IMU.
  //

  if (!IMU.begin()) {
    Serial.println("Failed to initialize IMU!");
    while (true);
  }
  Serial.println("IMU module initialized.");
  Serial.print("Accelerometer sample rate: ");
  Serial.print(IMU.accelerationSampleRate());
  Serial.println(" Hz.");
  Serial.println("Acceleration in g's: X\tY\tZ.");
  Serial.print("Gyroscope sample rate: ");
  Serial.print(IMU.gyroscopeSampleRate());
  Serial.println(" Hz.");
  Serial.println("Gyroscope in degrees/second: X\tY\tZ.");
}

void loop()
{
  static unsigned long checkWiFiTimeout = 0u; /**< The next value of the xyxtem clock, in milliseconds, where the WiFi connection has to be checked. */

  // Check WiFi connection every kCheckWiFiInterval milliseconds.
  {
    const unsigned long currentMillis = millis();
    if (currentMillis > checkWiFiTimeout)
    {
      TestWiFiConnection();
      checkWiFiTimeout = currentMillis + kCheckWiFiInterval;
    }
  }
  
  // The OSC message to send.
  OSCBundle messageBundle;

  // Reading IMU accelerator information.
  if (IMU.accelerationAvailable())
  {
    IMU.readAcceleration(imuSensorValues[kIMUXAxisAccelerationIndex], imuSensorValues[kIMUYAxisAccelerationIndex], imuSensorValues[kIMUZAxisAccelerationIndex]);
    AddSensorValues("IMU acceleration", kIMUXAxisAccelerationIndex, kIMUZAxisAccelerationIndex, messageBundle);
  }

  // Reading IMU gyroscope information.
  if (IMU.gyroscopeAvailable())
  {
    IMU.readGyroscope(imuSensorValues[kIMUXAxisGyroscopeIndex], imuSensorValues[kIMUYAxisGyroscopeIndex], imuSensorValues[kIMUZAxisGyroscopeIndex]);
    AddSensorValues("IMU gyroscope", kIMUXAxisGyroscopeIndex, kIMUZAxisGyroscopeIndex, messageBundle);
  }
  
  // Send OSC message only if IMU values were present.
  if (messageBundle.size() != 0u)
  {
    // Initialize UDP packet.
    //udp.beginPacket(kUDPDestinationIPAddress, kUDPDestinationPort);
    udp.beginPacket(kUDPBroadcastIPAddress, kUDPDestinationPort);
    // Send the bytes to the SLIP stream.
    messageBundle.send(udp);
    // Mark the end of the OSC Packet.
    udp.endPacket();
    // Free space occupied by message bundle.
    messageBundle.empty();

    // Notifying that a message was sent by blinking a LED.
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50);
    digitalWrite(LED_BUILTIN, LOW);
  }

  delay(20);
}

/**
 * FIXME: Add documentation.
 */
void AddSensorValues(const char* label, const uint8_t firstIndex, const uint8_t lastIndex, OSCBundle& outMessageBundle)
{
  Serial.print(label);
  Serial.print(": ");
  for (uint8_t index = firstIndex; index <= lastIndex; ++index)
  {
    outMessageBundle.add(kOSCAddresses[index]).add(imuSensorValues[index]);

    Serial.print('\t');
    Serial.print(imuSensorValues[index]);
  }    
  Serial.println();
}

/**
 * Test if the WiFi module is connected to a network. Start the connection if the WiFi module is not connected.
 */
void TestWiFiConnection()
{
  if (WiFi.status() == WL_CONNECTED)
    return;

  Serial.print("WiFi connection lost. Reconnecting to ");
  Serial.println(kWiFiSSID);
  StartWiFiConnection();
  Serial.print("Done!");
}

/**
 * Start a WiFi connection.
 */
void StartWiFiConnection()
{
  Serial.print("Connecting to network with SSID ");
  Serial.print(kWiFiSSID);
  Serial.print(":");

  int status = WL_DISCONNECTED;
  while (status != WL_CONNECTED)
  {
    status = WiFi.begin(kWiFiSSID, kWiFiPassword);

    // Tell the user the connection is in process.

    // On the terminal.
    Serial.print(" .");
    // By blinking a LED.
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000u);
    digitalWrite(LED_BUILTIN, LOW);

    // Wait for one seconds for the connection to start.
    delay(1000u);
  }

  Serial.println(" Done!");
}