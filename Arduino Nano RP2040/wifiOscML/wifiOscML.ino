#include <SPI.h>
#include <WiFi.h>
#include <WiFiUdp.h>
#include <Arduino_LSM6DS3.h>
#include <OSCMessage.h>
#include <OSCBundle.h>
#include <LSM6DSOXSensor.h>

// The DEV_I2C wire may change according to the Arduino version.
#ifdef ARDUINO_SAM_DUE
#define DEV_I2C Wire1
#elif defined(ARDUINO_ARCH_STM32)
#define DEV_I2C Wire
#elif defined(ARDUINO_ARCH_AVR)
#define DEV_I2C Wire
#else
#define DEV_I2C Wire
#endif

// Interruption alias.
#define INT_1 INT_IMU

// Led colors.
#define RED 22
#define BLUE 24
#define GREEN 23
#define LED_PWR 25

// = Wifi network related variables. ===

constexpr char* gSSID = "LuMu";//"Vodafone-A44725461";
constexpr char* gPassPhrase = "LuMu2024";//"H3pRc9nJkXh6mYhN";

// = UDP related variables. ===

// UDP connection object.
WiFiUDP Udp;

// Arduino UDP server port
constexpr int gArduinoUDPPort = 8000;

// Destination IP (PC)
const IPAddress gTouchDesignerIp(192,168,43,161);//192,168,1,4);
constexpr unsigned int gTouchDesignerPort = 9999;

// = Machine Learning related variables. ===

// Machine Learning sensor.
LSM6DSOXSensor AccGyr(&Wire, LSM6DSOX_I2C_ADD_L);

// Interruption flag: True if an event is present, false otherwise.
volatile bool isEventPresent = false;
// Interruption callback.
void INT1Event_cb();

// Sensor status.
typedef enum {
  FreeFall = 0u,          /**< Wakeup status. */
  TopLeftOrientation,     /**< Top-left orientation status. */
  TopRightOrientation,    /**< Top-right orientation status. */
  BottomLeftOrientation,  /**< Bottom-left orientation status. */
  BottomRightOrientation, /**< Bottom-right orientation status. */
  FaceUpOrientation,      /**< Face-up orientation status. */
  FaceDownOrientation,    /**< Face-down orientation status. */
  WakeUp,                 /**< Wakeup status. */
  UnhandledStatus         /**< Status not handled by this code. */

} SensorStatus;
// Sensor status OSC names.
const char* gSensorStatusOSCNames[] = {
  "/cade",
  "/orientation/top/left",
  "/orientation/top/right",
  "/orientation/bottom/left",
  "/orientation/bottom/right",
  "/orientation/face/up",
  "/orientation/face/down",
  "/wakeup"
};

void setup()
{
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(LED_PWR, OUTPUT);
  pinMode(BLUE, OUTPUT);

  // Initialize serial port.
  Serial.begin(9600);

  SetupWifiConnection(gSSID, gPassPhrase);
  SetupUDPServer(gArduinoUDPPort);
  SetupMachineLearningModule();
}

void loop()
{
  // Skip if no machine learning event is present.
  if (!isEventPresent)
    return;

  // Reset the event flag.
  isEventPresent = false;

  // Get machine learning event. We skip if there was an error reading the event.
  LSM6DSOX_Event_Status_t status;
  if (AccGyr.Get_X_Event_Status(&status) == LSM6DSOX_ERROR)
  {
    Serial.println("Unable to read X event status.");
    return;
  }

  const SensorStatus sensorStatus = GetSensorStatus(status);
  // Skip if we are not handling the sensor status.
  if (sensorStatus == UnhandledStatus)
    return;

  // Send the OSC message as a pulse.
  SendOSCMessage(gSensorStatusOSCNames[sensorStatus], 1);
  delay(300);
  SendOSCMessage(gSensorStatusOSCNames[sensorStatus], 0);
  delay(20);
}

void INT1Event_cb()
{
  isEventPresent = true;
}

void SetupWifiConnection(const char* ssid, const char* passPhrase)
{
  // Check if the WiFi module is present.
  if (WiFi.status() == WL_NO_MODULE)
  {
    Serial.println("Communication with WiFi module failed!");
    // Turn the LED on to let the user know about the failure.
    digitalWrite(LED_BUILTIN, HIGH);
    // Don't continue the execution.
    while (true);
  }

  // Connecting to WiFi network.
  do
  {
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);

    // We make the LED blink to let the user know about the connection attempt.
    digitalWrite(LED_BUILTIN, HIGH);
    delay(1000);
    digitalWrite(LED_BUILTIN, LOW);
    delay(10000);
    digitalWrite(LED_PWR, LOW);
  } while (WiFi.begin(ssid, passPhrase) != WL_CONNECTED);

  // At this point we are sucessfully connected the network. We print connection information.

  Serial.println("Connected!");
  // Network SSID.
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  // WiFi shield's IP address.
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  // Signal strength.
  Serial.print("Signal strength (RSSI):");
  Serial.print(WiFi.RSSI());
  Serial.println(" dBm");
}

void SetupUDPServer(const int port)
{
  Serial.println("Attempting to start UDP server...");
  // if you get a connection, report back via serial:
  if (!Udp.begin(port))
  {
    Serial.println("UDP server initialization failed!");
    // Don't continue the execution.
    while (true);
  }
  Serial.println("Done!");
}

void SetupMachineLearningModule()
{
  Serial.println("Attempting to start Machine Learning module...");

  // Force INT1 of LSM6DSOX low in order to enable I2C
  pinMode(INT_1, OUTPUT);
  digitalWrite(INT_1, LOW);
  delay(200);

  // Initialize I2C bus.
  Wire.begin();

  // Setup ML module.
  if (AccGyr.begin() == LSM6DSOX_ERROR)
  {
    Serial.println("Machine Learning module initialization failed!");
    // Don't continue the execution.
    while (true);
  }

  // Enable gyroscope sensor.
  if (AccGyr.Enable_X() == LSM6DSOX_ERROR)
  {
    Serial.println("Unable to enable Machine Learning module's X sensor!");
    while (true);
  }
  
  // Enable free fall detection.
  if (AccGyr.Enable_Free_Fall_Detection(LSM6DSOX_INT1_PIN) == LSM6DSOX_ERROR)
  {
    Serial.println("Unable to enable ree fall detection!");
    while (true);
  }

  // Enable 6D orientation detection.
  if (AccGyr.Enable_6D_Orientation(LSM6DSOX_INT1_PIN) == LSM6DSOX_ERROR)
  {
    Serial.println("Unable to enable 6D orientation detection!");
    while (true);
  }

  // Enable wake up detection.
  // if (AccGyr.Enable_Wake_Up_Detection(LSM6DSOX_INT1_PIN) == LSM6DSOX_ERROR)
  // {
  //   Serial.println("Unable to enable wake up detection!");
  //   while (true);
  // }

  // Setup interrupts.
  pinMode(INT_1, INPUT);
  attachInterrupt(INT_1, INT1Event_cb, RISING);
  // We need to wait for a time window before having the first MLC status.
  delay(3000);

  Serial.println("Done!");
}

SensorStatus GetSensorStatus(const LSM6DSOX_Event_Status_t& status)
{
  if (status.FreeFallStatus)
  {
    Serial.println("Free Fall detected!");
    return FreeFall;
  }

  if (status.D6DOrientationStatus)
  {
    uint8_t xl = 0;
    uint8_t xh = 0;
    uint8_t yl = 0;
    uint8_t yh = 0;
    uint8_t zl = 0;
    uint8_t zh = 0;

    AccGyr.Get_6D_Orientation_XL(&xl);
    AccGyr.Get_6D_Orientation_XH(&xh);
    AccGyr.Get_6D_Orientation_YL(&yl);
    AccGyr.Get_6D_Orientation_YH(&yh);
    AccGyr.Get_6D_Orientation_ZL(&zl);
    AccGyr.Get_6D_Orientation_ZH(&zh);

    if ( xl == 0 && yl == 0 && zl == 0 && xh == 0 && yh == 1 && zh == 0 )
    {
      Serial.println("Top-left orientation detected!");
      return TopLeftOrientation;
    }      

    if ( xl == 1 && yl == 0 && zl == 0 && xh == 0 && yh == 0 && zh == 0 )
    {
      Serial.println("Top-right orientation detected!");
      return TopRightOrientation;
    }
      
    if ( xl == 0 && yl == 0 && zl == 0 && xh == 1 && yh == 0 && zh == 0 )
    {
      Serial.println("Bottom-left orientation detected!");
      return BottomLeftOrientation;
    }

    if ( xl == 0 && yl == 1 && zl == 0 && xh == 0 && yh == 0 && zh == 0 )
    {
      Serial.println("Bottom-right orientation detected!");
      return BottomRightOrientation;
    }     

    if ( xl == 0 && yl == 0 && zl == 0 && xh == 0 && yh == 0 && zh == 1 )
    {
      Serial.println("Face-up orientation detected!");
      return FaceUpOrientation;
    } 

    if ( xl == 0 && yl == 0 && zl == 1 && xh == 0 && yh == 0 && zh == 0 )
    {
      Serial.println("Face-down orientation detected!");
      return FaceDownOrientation;
    }
            
    Serial.println("None of the 6D orientation axes is set in LSM6DSO - accelerometer.");
    return UnhandledStatus;
  }

  // if (status.WakeUpStatus)
  // {
  //   Serial.println("Wake up detected!");
  //   return WakeUp;
  // }

  // The status is not currently handled.
  return UnhandledStatus;
}

void SendOSCMessage(const char* name, const int value)
{
  // The OSC message bundle to send.
  OSCBundle message;
  message.add(name).add(value); 

  // Start UDP transmission.
  Udp.beginPacket(gTouchDesignerIp, gTouchDesignerPort);
  // Send the bytes to the SLIP stream.
  message.send(Udp);
  // Mark the end of the OSC packet.
  Udp.endPacket();
  // Free space occupied by message.
  message.empty();  
}
