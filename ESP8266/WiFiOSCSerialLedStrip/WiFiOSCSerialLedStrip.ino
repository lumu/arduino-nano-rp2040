/**
 * FIXME: Add licensing information.
 */

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <OSCBundle.h>
#include <OSCMessage.h>
#include <WS2812FX.h>

#include "OSCMessageAddresses.h"

#define LUMU_DEBUG

// Mark: Input processing related.

constexpr const unsigned long kTimerInterval = 150u; /**< The time interval to wait between LED strip updates. */
unsigned long timerUpperBound = 0u;                  /**< The timer value after which the LED strip is updated. */

void SetIMUAxValue(OSCMessage& message); /**< The function callback to set the value of IMU X axis acceleration value. */

// Mark: WiFi related.

constexpr const char* kWiFiSSID = "lumu";         /**< The WiFi network ssid. */
constexpr const char* kWiFiPassword = "lumu2023"; /**< The WiFi network password. */
constexpr uint32_t kUDPLocalPort = 8000u;         /**< The local port for UDP messages. */

WiFiUDP udp; /**< The object to handle UDP connections. */

// Mark: LED Strip related.

constexpr uint16_t kLEDCount = 150u;                     /**< The number of LEDs on the strip. */
constexpr uint8_t kLEDPin = 4u;                          /**< The pin number on the board to send commands to the LED strip. */
constexpr uint16_t kInitialLEDStripSpeed = 666u;         /**< The initial speed of the LED strip. */
constexpr uint32_t kInitialLEDStripColor = BLUE;         /**< The initial color of the LED strip. */
constexpr uint8_t kInitialLEDStripBrightness = 55;       /**< The initial brightness of the LED strip. */
constexpr uint8_t kInitialLEDStripMode = FX_MODE_STATIC; /**< The initial mode of the LED strip. */

WS2812FX ws2812fx = WS2812FX(kLEDCount, kLEDPin, NEO_GRB + NEO_KHZ800); /**< The LED strip library handling object. */

// Mark: OSC handling related.

float imuSensorValues[6u]; /**< The values of the IMU sensors. Acceleration values are in Hertz. Gyroscope values in units of standard gravity. */

void SetIMUAxValue(OSCMessage& message);    /**< The function callback to set the value of IMU X axis acceleration value. */
void SetIMUAyValue(OSCMessage& message);    /**< The function callback to set the value of IMU Y axis acceleration value. */
void SetIMUAzValue(OSCMessage& message);    /**< The function callback to set the value of IMU Z axis acceleration value. */
void SetIMUGxValue(OSCMessage& message);    /**< The function callback to set the value of IMU X axis gyroscope value. */
void SetIMUGyValue(OSCMessage& message);    /**< The function callback to set the value of IMU Y axis gyroscope value. */
void SetIMUGzValue(OSCMessage& message);    /**< The function callback to set the value of IMU Z axis gyroscope value. */

void SetLEDStripMode(OSCMessage& message);       /**< The function callback to set the LED strip mode. */
void SetLEDStripColor(OSCMessage& message);      /**< The function callback to set the LED strip color. */
void SetLEDStripBrightness(OSCMessage& message); /**< The function callback to set the LED strip brightness. */
void SetLEDStripSpeed(OSCMessage& message);      /**< The function callback to set the LED strip speed. */

void (*const kOSCCallBacks[])(OSCMessage& message) = {
  SetIMUAxValue, SetIMUAyValue, SetIMUAzValue,
  SetIMUGxValue, SetIMUGyValue, SetIMUGzValue,
  SetLEDStripMode, SetLEDStripColor, SetLEDStripBrightness, SetLEDStripSpeed }; /**< An array to hold the function callbacks. */

// Mark: Sketch content.

void setup()
{
  // Initialize serial port.
  Serial.begin(9600u);
  delay(1000);

  //
  // Initializing WiFi connection.
  //

  if (!WiFi.softAP(kWiFiSSID, kWiFiPassword))
  {
    Serial.println("Unable to start WiFi access point!");

    // Suspend the program execution.
    while (true);
  }

  Serial.println("WiFi access point started.");
  Serial.print("IP address: ");
  Serial.println(WiFi.softAPIP());

  if (!WiFi.softAPDhcpServer().isRunning())
  {
    Serial.println("DHCP server is not running!");
    while (true);
  }

  Serial.println("DHCP server running.");

  //
  // Initializing UDP client.
  //

  if (!udp.begin(kUDPLocalPort))
  {
    Serial.println("Unable to initialize UDP socket!");
    digitalWrite(LED_BUILTIN, HIGH);
    while (true);
  }
  Serial.print("UDP socket initialized on port ");
  Serial.println(kUDPLocalPort);

  //
  // Initializing LED strip library.
  //

  ws2812fx.init();
  ws2812fx.setBrightness(kInitialLEDStripBrightness);
  ws2812fx.setSpeed(kInitialLEDStripSpeed);
  ws2812fx.setColor(kInitialLEDStripColor);
  ws2812fx.setMode(kInitialLEDStripMode);
  ws2812fx.start();
}

void loop()
{
  // Execute the step for the current mode.
  ws2812fx.service();

  // Skip if less than kTimerInterval milliseconds have passed.
  if (millis() <= timerUpperBound)
    return;

  // Process messages only if there is data available.
  const int packetSize = udp.parsePacket();
  if (packetSize > 0)
  {
    // The OSC message bundle.
    OSCBundle bundle;

    // Fill the bundle with the content of the UDP message.
    for (int readCount = packetSize; readCount > 0; readCount--)
    {
      bundle.fill(udp.read());
    }

    // Extract the IMU values if there was no errors filling the bundle.
    if (bundle.hasError())
    {
      Serial.print("Unable to read the OSC message! Error: ");
      Serial.println(bundle.getError());
    }
    else
    {
      for (uint8_t index; index < kOSCAddressCount; ++index)
      {
        bundle.dispatch(kOSCAddresses[index], kOSCCallBacks[index]);
      }
    }
  }

#if defined(LUMU_DEBUG)
  PrintOSCVariableValues();
#endif

  // Updating the timer upper bound.
  timerUpperBound = millis() + kTimerInterval;
}

/**
 * Function callbacks to set up the values of the IMU sensors.
 *
 * @param [in] message The OSC message that contains the sensor value.
 */
void SetIMUAxValue(OSCMessage& message) { imuSensorValues[kIMUXAxisAccelerationIndex] = message.getFloat(0); }
void SetIMUAyValue(OSCMessage& message) { imuSensorValues[kIMUYAxisAccelerationIndex] = message.getFloat(0); }
void SetIMUAzValue(OSCMessage& message) { imuSensorValues[kIMUZAxisAccelerationIndex] = message.getFloat(0); }
void SetIMUGxValue(OSCMessage& message) { imuSensorValues[kIMUXAxisGyroscopeIndex] = message.getFloat(0); }
void SetIMUGyValue(OSCMessage& message) { imuSensorValues[kIMUYAxisGyroscopeIndex] = message.getFloat(0); }
void SetIMUGzValue(OSCMessage& message) { imuSensorValues[kIMUZAxisGyroscopeIndex] = message.getFloat(0); }

/**
 * Function callback to set up the LED strip mode.
 * 
 * @param [in] message The OSC message to process.
 */
void SetLEDStripMode(OSCMessage& message)
{
  const int mode = message.getInt(0);
  if (mode < 0 || mode >= ws2812fx.getModeCount())
  {
    Serial.print("Unkown LED strip mode.");
    return;
  }

  ws2812fx.stop();
  ws2812fx.setMode((uint8_t)mode);
  ws2812fx.start();
}

/**
 * Function callback to set up the LED strip color.
 * 
 * @param [in] message The input OSC message to process.
 */
void SetLEDStripColor(OSCMessage& message)
{
  if (message.size() != 3)
  {
    Serial.print("Incorrect number of elements.");
    return;
  }

  const int red = message.getInt(0);
  const int green = message.getInt(1);
  const int blue = message.getInt(2);
  if (red < 0 || red > 255 || green < 0 || green > 255 || blue < 0 || blue > 255)
  {
    Serial.print("Color component out of bounds.");
    return;
  }

  ws2812fx.setColor((uint8_t)red, (uint8_t)green, (uint8_t)blue);
}

/**
 * Function callback to set up the LED strip brightness.
 *
 * @param [in] message The OSC message to process.
 */
void SetLEDStripBrightness(OSCMessage& message)
{
  const int brightness = message.getInt(0);
  if (brightness < 0 || brightness > 255)
  {
    Serial.print("Brightness out of bounds.");
    return;
  }

  ws2812fx.setBrightness((uint8_t)brightness);
}

/**
 * Function callback to set up the LED strip velocity.
 *
 * @param [in] message The OSC message to process.
 */
void SetLEDStripSpeed(OSCMessage& message)
{
  const int speed = message.getInt(0);
  if (speed < 0 || speed > 255)
  {
    Serial.print("Velocity out of bounds.");
    return;
  }

  ws2812fx.setSpeed((uint8_t)speed);
}

/**
 * Print the values of the OSC variables for debug purposes.
 */
void PrintOSCVariableValues()
{
  Serial.print("IMU sensor values:");
  for (const float& sensorValue : imuSensorValues)
  {
    Serial.print('\t');
    Serial.print(sensorValue);
  }
  Serial.println();
}