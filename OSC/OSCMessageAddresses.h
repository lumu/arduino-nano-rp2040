/**
 * FIXME: Add Licensing information.
 */

#ifndef LUMU_OSCMessageAddresses
#define LUMU_OSCMessageAddresses

constexpr uint8_t kOSCAddressCount = 10u; /**< The number of OSC message addresses. */

constexpr uint8_t kIMUXAxisAccelerationIndex = 0u; /**< The index in \p kOSCAddresses of the IMU X axis acceleration. */
constexpr uint8_t kIMUYAxisAccelerationIndex = 1u; /**< The index in \p kOSCAddresses of the IMU Y axis acceleration. */
constexpr uint8_t kIMUZAxisAccelerationIndex = 2u; /**< The index in \p kOSCAddresses of the IMU Z axis acceleration. */
constexpr uint8_t kIMUXAxisGyroscopeIndex = 3u;    /**< The index in \p kOSCAddresses of the IMU X axis gyroscope. */
constexpr uint8_t kIMUYAxisGyroscopeIndex = 4u;    /**< The index in \p kOSCAddresses of the IMU Y axis gyroscope. */
constexpr uint8_t kIMUZAxisGyroscopeIndex = 5u;    /**< The index in \p kOSCAddresses of the IMU Z axis gyroscope. */
constexpr uint8_t kLEDStripModeIndex = 6u;         /**< The index in \p kOSCAddresses of the LED strip mode. */
constexpr uint8_t kLEDStripColor = 7u;             /**< The index in \p kOSCAddresses of the LED strip color. */
constexpr uint8_t kLEDStripBrightness = 8u;        /**< The index in \p kOSCAddresses of the LED strip brightness. */
constexpr uint8_t kLEDStripSpeed = 9u;             /**< The index in \p kOSCAddresses of the LED strip speed. */

constexpr const char* kOSCAddresses[] =
{
  "/imu/ax",         /**< The OSC address of the IMU X axis acceleration. */
  "/imu/ay",         /**< The OSC address of the IMU Y axis acceleration. */
  "/imu/az",         /**< The OSC address of the IMU Z axis acceleration. */
  "/imu/gx",         /**< The OSC address of the IMU X axis gyroscope. */
  "/imu/gy",         /**< The OSC address of the IMU Y axis gyroscope. */
  "/imu/gz",         /**< The OSC address of the IMU Z axis gyroscope. */
  "/led/mode",       /**< The OSC address of the LED strip mode. */
  "/led/color",      /**< The OSC address of the LED strip color. */
  "/led/brightness", /**< The OSC address of the LED strip brightness. */
  "/led/speed"       /**< The OSC address of the LED strip speed. */
};

#endif